# Web Application: Share Experience

This is the Share Experience web application using Ruby on Rails


## Developed by Team of 5
- Basma Mahmoud Taha
- Amira Mohamed Fathi
- Gehad Fathi Mohamed
- Aliaa Osman
- Radwa Adel El-Masry

## The Project main objective
It is developed for learning purposes using Ruby on Rails.

## The application main Features:
- Log in/out
- User profile.
- Posting / Commenting / Searching / Sharing.